<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject_id',
        'folder',
        'data',
    ];

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }
}
