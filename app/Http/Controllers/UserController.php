<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    private $authUser = null;

    public function __construct()
    {
        $this->authUser = Auth::user();
        $this->beforeFilter(function()
        {
            if ($this->authUser->level > 1) return response('Bad Request!', 400);
        });
    }

    public function index()
    {
        $users = User::where('level', '>=', $this->authUser->level)->orWhere('level', 0)->paginate(10);

        return view('user.index', [
            'user' => $this->authUser,
            'users' => $users
        ]);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request  $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required|min:8',
            'email' => 'required|email|unique:users'
        ]);

        if ($validator->fails())
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();

        $user = User::create($request->all());
        $user->password = bcrypt($request->input('password'));

        if ($user->save()) {
            return redirect('user')->with('message', trans('user.create_success'));
        } else {
            return response('Server Error!', '500');
        }
    }

    public function update(Request $request, $id)
    {
        if (! $this->authUser->level == 1) return response('Bad Request!', 400);

        $user = User::find($id);

        if (! $user) return response('Bad Request!', 400);

        $data = $request->input('email') === $user->email ? $request->except('email') : $request->all();

        $validator = Validator::make($data, [
            'email' => 'email|unique:users'
        ]);

        if ($validator->fails())
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();

        if (! $user) return response(trans('user.not_exist'), 400);

        $user->fill($data);

        return $user->save() ? redirect()->back()->with('message', trans('user.update_success')) :
            response('Server Error!', '500');
    }

    public function destroy($id)
    {
        if (! $this->authUser->level == 1) return response('Bad Request!', 400);

        $user = User::find($id);

        if (! $user) return response('Bad Request!', 400);

        $result = $user->delete();

        return $result ? redirect()->back()->with('message', trans('user.delete_success')) : response('Server Error!', '500');
    }
}
