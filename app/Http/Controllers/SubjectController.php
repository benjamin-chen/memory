<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Validator;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::paginate(10);
        return view('subject.index', ['subjects' => $subjects]);
    }

    public function create()
    {
        return view('subject.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'birth_date' => 'required',
        ]);

        if ($validator->fails())
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();

        $subject = Subject::create($request->all());

        if ($subject->save()) {
            return redirect('subject')->with('message', trans('member.create_success'));
        } else {
            return response('Server Error!', '500');
        }
    }

    public function show()
    {
        return view('subject.show');
    }

    public function edit($id)
    {
        $subject = Subject::find($id);

        if (! $subject) return response('Bad Request!', 400);

        return view('subject.edit', [
            'subject' => $subject
        ]);
    }

    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);

        if (! $subject) return response('Bad Request!', 400);

        $subject->fill($request->all());

        if ($subject->save()) {
            return redirect("subject/{$subject->id}/edit")->with('message', trans('member.updated_success'));
        } else {
            return response('Server Error!', '500');
        }
    }

    public function destroy($id)
    {
        $subject = Subject::find($id);

        if (! $subject) return response('Bad Request!', 400);

        $result = $subject->delete();

        return $result ? redirect()->back()->with('message', trans('member.delete_success')) : response('Server Error!', '500');
    }
}
