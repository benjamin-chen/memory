<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo_id',
        'name',
        'birth_date',
    ];

    public function publications()
    {
        return $this->hasOne('App\Photo');
    }
}