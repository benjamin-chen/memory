@extends('layout.basic')

@section('title', 'MCG-Subject')

@section('header')
    @include('component.header')
@endsection

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-info">
            {{ Session::get('message') }}
        </div>
    @endif

    <table class="table table-striped table-hover ">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>birth_date</th>
                <th>created_at</th>
            </tr>
        </thead>
        <tbody>
            @foreach($subjects as $subject)
            <tr>
                <td>{{ $subject->id }}</td>
                <td>
                    <a href="/subject/{{ $subject->id }}">{{ $subject->name }}</a>
                </td>
                <td>{{ $subject->birth_date }}</td>
                <td>{{ $subject->created_at }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="6">{!! $subjects->render() !!}</td>
            </tr>
        </tbody>
    </table>

@endsection