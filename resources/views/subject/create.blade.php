@extends('layout.basic')

@section('title', 'MCG-Subject')

@section('header')
    @include('component.header')
@endsection

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-info">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="well bs-component">
        <form class="form-horizontal" id="subject-create" method="post" action="/subject">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <fieldset>
                <legend>新增受試者</legend>
                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">姓名</label>

                    <div class="col-md-10">
                        <input name="name" id="name" class="form-control" placeholder="王小明  " value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="birth" class="col-md-2 control-label">生日</label>

                    <div class="col-md-10">
                        <input name="birth_date" type="date" class="form-control" id="birth">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">建立使用者</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

@endsection
