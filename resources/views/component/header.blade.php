<div class="bs-component">
    <div class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">MCG</a>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <?php $user = Auth::user();?>
                @if (! empty($user))
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Subject
                            <b class="caret"></b>
                            <div class="ripple-container"></div>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/subject/create">Create Subject</a></li>
                            <li><a href="/subject">Manage Subject</a></li>
                        </ul>
                    </li>
                    @if($user->level == 1)
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            User
                            <b class="caret"></b>
                            <div class="ripple-container"></div>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/user/create">Create User</a></li>
                            <li><a href="/user">Manage User</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/logout">Logout</a></li>
                </ul>
                @else
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/auth/login">Login</a></li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
