@extends('layout.basic')

@section('title', 'APAR - Profile')

@section('header')
    @include('component.header')
@endsection

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-info">
            {{ Session::get('message') }}
        </div>
    @endif

    <table class="table table-striped table-hover ">
        <thead>
            <tr>
                <th>id</th>
                <th>e-mail</th>
                <th>name</th>
                <th>level</th>
                @if ($user->level == 1)
                <th>Operate</th>
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($users as $u)
            <tr>
                @if ($user->level == 1)
                <form id="update-{{ $u->id }}" action="/user/{{ $u->id }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="put" />
                    <td>{{ $u->id }}</td>
                    <td><input name="email" class="form-control" value="{{ $u->email }}"></td>
                    <td><input name="name" class="form-control" value="{{ $u->name }}"></td>
                    <td>
                        <select name="level" id="level" class="form-control">
                            <option value="1" @if($u->level == 1) selected @endif>1</option>
                            <option value="2" @if($u->level == 2) selected @endif>2</option>
                        </select>
                    </td>
                </form>
                <td>
                    <button form="update-{{ $u->id }}" class="btn btn-raised btn-success">Edit</button>
                </td>
                <td>
                    <form action="/user/{{ $u->id }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="delete" />
                        <input class="btn btn-raised btn-danger" type="submit" value="Delete">
                    </form>
                </td>
                @else
                <td>{{ $u->id }}</td>
                <td>{{ $u->email }}</td>
                <td>{{ $u->name }}</td>
                <td>{{ $u->level }}</td>
                @endif
            </tr>
            @endforeach
            <tr>
                <td colspan="6">{!! $users->render() !!}</td>
            </tr>
        </tbody>
    </table>

@endsection